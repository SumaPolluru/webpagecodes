package com.app.ContactManagerApplication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.app.ContactManagerApplication.model.User;
import com.app.ContactManagerApplication.service.UserService;
@org.springframework.web.bind.annotation.RestController
public class RegisterationController {
	
	@Autowired
	private UserService userService;

	@GetMapping("/")
	public String hello() {
		return "This is Home page";
	}
	
	
	@GetMapping("/saveuser")
	public String saveUser(@RequestParam String userName, @RequestParam String emailId,@RequestParam String password) {
		User user = new User(userName,emailId, password);
		userService.saveMyUser(user);
		return "User Saved";
	}
}
