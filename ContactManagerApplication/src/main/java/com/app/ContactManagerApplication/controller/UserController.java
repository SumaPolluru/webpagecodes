package com.app.ContactManagerApplication.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.app.ContactManagerApplication.model.Contact;
import com.app.ContactManagerApplication.model.User;
import com.app.ContactManagerApplication.repositories.UserRepository;


@Controller
public class UserController {
	@Autowired
	UserRepository usrep;

	@RequestMapping("/CountData")
	public String getQuery() {
		List<Long> clist = usrep.getConList(2);
		for (Long e1 : clist) {
			System.out.println("Count " + e1);
		}
		return "success";
	}

	@RequestMapping("/MobileNumberQuery")
	public String getCon() {
		List<Contact> clist = usrep.getContacts();
		if (clist != null) {
			for (Contact e1 : clist) {
				System.out.println("Contact Id " + e1.getContactId());
				System.out.println("Contact First Name: " + e1.getFirstName());
				System.out.println("Contact Last Name: " + e1.getLastName());
				System.out.println("Email Id: " + e1.getEmailId());
				;
				System.out.println("Mobile Number :" + e1.getMobileNumber());
			}

		}
		return "success";
	}

	@RequestMapping("/AjayQuery")
	public String getAjayDetails() {
		List<Contact> clist = usrep.getAjayData();
		if (clist != null) {
			for (Contact e1 : clist) {
				System.out.println("Contact Id " + e1.getContactId());
				System.out.println("Contact First Name: " + e1.getFirstName());
				System.out.println("Contact Last Name: " + e1.getLastName());
				System.out.println("Email Id: " + e1.getEmailId());
				;
				System.out.println("Mobile Number :" + e1.getMobileNumber());
			}

		}
		return "success";
	}

	@RequestMapping("/AjayDelhiQuery")
	public String getAjayD() {
		List<Contact> clist = usrep.getAjayDelhiData();
		if (clist != null) {
			for (Contact e1 : clist) {
				System.out.println("Contact Id " + e1.getContactId());
				System.out.println("Contact First Name: " + e1.getFirstName());
				System.out.println("Contact Last Name: " + e1.getLastName());
				System.out.println("Email Id: " + e1.getEmailId());
				System.out.println("Mobile Number :" + e1.getMobileNumber());
			}

		}
		return "success";
	}
	
	@RequestMapping("/deleteByuserId") //Contacts also automatically deleted
	public ModelAndView deleteByUserId() 
	{

		return new ModelAndView("deleteUserForm", "uobj", new User());
	}

	@RequestMapping("/deleteUser")
	public ModelAndView deleteUser(@ModelAttribute("uobj") User user) 
	{
		usrep.deleteById(user.getUserId());
		return new ModelAndView("success");
	}
}
	
	/*@RequestMapping("/deleteUserContacts") //Tried using query but not supported
	public String getdeletionStatus()
	{
		List<Contact>clist = usrep.getDeleteId(2);
		if (clist == null) 
		{
		System.out.println("Successfully deleted");
		}
		return "success";
		}
		*/