package com.app.ContactManagerApplication.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.app.ContactManagerApplication.model.Contact;
import com.app.ContactManagerApplication.repositories.ContactRepository;
import com.app.ContactManagerApplication.repositories.UserRepository;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Controller
public class ContactController {
	@Autowired
	ContactRepository conrep;
	/*@Autowired
	AddressRepository adrep;*/
	@Lazy
	@Autowired
	UserRepository usrep;
	
	@ApiOperation(value = "Contact List", 
			notes="This method will retreive list from database",nickname = "getContacts")
	         @ApiResponses(value = {
		        @ApiResponse(code = 500, message = "Server error"),
		         @ApiResponse(code = 404, message = "Contact not found"),
		        @ApiResponse(code = 200, message = "Successful retrieval",
		            response = Contact.class, responseContainer = "List") })

	@RequestMapping("/")
	public String welcome() {
		return "welcomepage";
	}

	@RequestMapping("/InsertContact")
	public ModelAndView getForm()
		{
			List<Integer> userIdlist=usrep.getUser();
			//arrlist.add("one");
			//arrlist.add("two");
			//arrlist.add("three");
			//arrlist=emprep.getEmpList();
			Contact con1=new Contact();
			con1.setUserIdlist(userIdlist);
		return new ModelAndView("contactForm", "contactdata", con1);
	}

	@RequestMapping("/addContact")
	public String data(@ModelAttribute("contactdata") Contact con) {
		conrep.save(con);// it will insert data in database.
		return "success";
	}

	@RequestMapping("/RetrieveDetails")
	public ModelAndView getDetails() {
		List<Contact> conlist = conrep.findAll();
		return new ModelAndView("details", "cdata", conlist);
	}

	@RequestMapping("/GetById")
	public ModelAndView getEmpDetails() {
		return new ModelAndView("retrievebyIdForm", "coniddata", new Contact());
	}

	@RequestMapping("/update")
	public ModelAndView update(@ModelAttribute("coniddata") Contact e) {

		Optional<Contact> cobj = conrep.findById(e.getContactId());// it will insert data in databse.
		Contact efind = null;
		if (cobj.isPresent()) {
			efind = cobj.get();
		}
		return new ModelAndView("updateForm", "updateobj", efind);

	}

	@RequestMapping("/saveUpdate")
	public ModelAndView saveUpdates(@ModelAttribute("updateobj") Contact contact) {
		conrep.save(contact);
		return new ModelAndView("success");
	}

	@RequestMapping("/deleteById")
	public ModelAndView deleteById() {

		return new ModelAndView("deleteForm", "con", new Contact());
	}

	@RequestMapping("/delete")
	public ModelAndView deletedata(@ModelAttribute("con") Contact contact) {
		conrep.deleteById(contact.getContactId());
		return new ModelAndView("success");
	}

	/*@RequestMapping("/UserToAddress")
	public String addDetails() {
		Address adrobj = new Address();
		adrobj.setAddressId(1001);
		adrobj.setFlatNumber("13-132 B");
		adrobj.setArea("Puttur");
		adrobj.setCity("Tirupati");
		adrobj.setZipcode(517583);
		User uobj = new User();
		uobj.setUserId(100001);
		uobj.setUserName("Chintu");
		uobj.setPassword("Chintu12");
		uobj.setEmailId("Chintu@gmail.com");
		uobj.setAddobj(adrobj);
		adrep.save(adrobj);
		return "entity";
	}

	@RequestMapping("/oneToMany")
	public String saveDetails() {
		User u1 = new User();
		u1.setUserId(10001);
		u1.setUserName("Chintu");
		u1.setPassword("Chintu12");
		u1.setEmailId("Chintu@gmail.com");
		Contact c = new Contact(2001, "Sree", "Vani", "9387891721", "8607802813", "Vani@gmail.com", "Delhi");
		Contact c1 = new Contact(2002, "Chotu", "Reddy", "9566244721", "4678789713", "Chotu@gmail.com", "Mumbai");
		Contact c2 = new Contact(2003, "Riya", "shetty", "8687891721", "9678982813", "Riya@gmail.com", "Banglore");
		Contact c3 = new Contact(2004, "Tina", "sai", "9387891721", "3456767813", "Tina@gmail.com", "Chennai");
		List<Contact> elist = new ArrayList<Contact>();
		elist.add(c);
		elist.add(c1);
		elist.add(c2);
		elist.add(c3);
		u1.setContactList(elist);
		usrep.save(u1);
		return "success";
	}*/
	
	
	@RequestMapping("/retrieveOrderByfirstname")
	public String getSortedData()
	{	
	List<Contact> conlist=conrep.findByOrderByFirstNameAsc();
	if(conlist!=null)
	{
		for(Contact e1:conlist)
		{
			System.out.println("Contact Id: "+ e1.getContactId());
			System.out.println("Contact first Name: "+e1.getFirstName());
			System.out.println("Contact last Name: "+e1.getLastName());
			System.out.println("Contact: "+e1.getMobileNumber());
			System.out.println("Office Number: "+e1.getOfficeNumber());
			System.out.println("Email Id: "+e1.getEmailId());
			System.out.println("City: "+e1.getCity());
		}
	
	}
	return "success";
	}
	
	@RequestMapping("/retrieveQuery")
	public String getQuery()
	{	
	List<String> clist=conrep.findAllOrderByLastNameAsc();
	if(clist!=null)
	{
		for(String e1:clist)
		{
			
			System.out.println("Contact Name: "+e1);
		}
	
	}
	return "success";
}
	
	
}
