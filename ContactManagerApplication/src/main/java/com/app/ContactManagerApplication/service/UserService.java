package com.app.ContactManagerApplication.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.app.ContactManagerApplication.model.User;
import com.app.ContactManagerApplication.repositories.UserRepository;

@Service
@Transactional
public class UserService {
	
	private final UserRepository userRepository;
	
	public UserService(UserRepository userRepository) {
		this.userRepository=userRepository;
	}
	
	public void saveMyUser(User user ) {
		userRepository.save(user);
	}
	
	public User findByUsernameAndPassword(String emailId, String password) {
		return userRepository.findByEmailIdAndPassword(emailId, password);
	}
	
	}

	