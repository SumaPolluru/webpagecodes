package com.mycontroller.dao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.mycontroller.pojo.UserContacts;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

public class UserContactDAO {
	JdbcTemplate template;

	public void setTemplate(JdbcTemplate template) {
		this.template = template;
	}

	public int save(UserContacts p) {
		String sql = "insert into usercontacts " + "values(" + p.getUserId() + ",'" + p.getContactFirstName() + "'," + p.getContactLastName() + ",'"
				+ p.getEmail()+ "'," + p.getMobileno()  + "'," + p.getDate()+ "')";
		return template.update(sql);
	}

	public int update(UserContacts p) {
		String sql = "update usercontacts set contactFirstName='" + p.getContactFirstName() + "', contactLastName=" + p.getContactLastName() + ",email='"
				+ p.getEmail() + "', mobileno=" + p.getMobileno()+ "', date=" + p.getDate() + "' where userId=" + p.getUserId() + "";
		return template.update(sql);
	}

	public int delete(int userId) {
		String sql = "delete from usercontacts where userId=" + userId + "";
		return template.update(sql);
	}

	public UserContacts getEmpById(int userId) {
		String sql = "select * from usercontacts where userId=?";
		return template.queryForObject(sql, new Object[] { userId }, new BeanPropertyRowMapper<UserContacts>(UserContacts.class));
	}

	public List<UserContacts> getContacts() {
		return template.query("select * from userContacts", new RowMapper<UserContacts>() {
			public UserContacts mapRow(ResultSet rs, int row) throws SQLException {
				UserContacts e = new UserContacts();
				e.setUserId(rs.getInt(1));
				e.setContactFirstName(rs.getString(2));
				e.setContactLastName(rs.getString(3));
				e.setEmail(rs.getString(4));
				e.setMobileno(rs.getLong(5));
				e.setDate(rs.getString(6));
				return e;
			}
		});
	}
}