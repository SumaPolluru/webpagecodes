package com.mycontroller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mycontroller.dao.UserContactDAO;
import com.mycontroller.pojo.UserContacts;


@Controller
public class UserContactController {
	@Autowired
	UserContactDAO dao;
	
	@RequestMapping("/contactform")
	public String showform(Model m) {
		m.addAttribute("command", new UserContacts());
		return "contactform";
	}


	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(@ModelAttribute("emp") UserContacts emp) {
		dao.save(emp);
		return "redirect:/RetrieveContact";
	}

	
	@RequestMapping("/RetrieveContact")
	public String viewemp(Model m) {
		List<UserContacts> list = dao.getContacts();
		m.addAttribute("list", list);
		return "RetrieveContact";
	}

	@RequestMapping(value = "/editemp/{userId}")
	public String edit(@PathVariable int userId, Model m) {
	 UserContacts emp = dao.getEmpById(userId);
		m.addAttribute("command", emp);
		return "updateContact";
	}

	
	@RequestMapping(value = "/editsave", method = RequestMethod.POST)
	public String editsave(@ModelAttribute("emp") UserContacts emp) {
		dao.update(emp);
		return "redirect:/RetrieveContact";
	}

	@RequestMapping(value = "/deleteemp/{userId}", method = RequestMethod.GET)
	public String delete(@PathVariable int userId) {
		dao.delete(userId);
		return "redirect:/RetrieveContact";
	}
}