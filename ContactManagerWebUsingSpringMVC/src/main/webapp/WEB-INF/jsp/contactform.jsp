<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>NEW CONTACT</title>
</head>
<body>
	<h1>Add New Contact</h1>
	<form:form method="post" action="save">
		<table>
			<tr>
				<td></td>
				<td><form:hidden path="userId" /></td>
			</tr>
			<tr>
				<td>User Id:</td>
				<td><form:input path="userId" /></td>
			</tr>
			<tr>
				<td>Contact First Name :</td>
				<td><form:input path="contactFirstName" /></td>
			</tr>
			<tr>
				<td>Contact Last Name :</td>
				<td><form:input path="contactLastName" /></td>
			</tr>
			<tr>
				<td>Email:</td>
				<td><form:input path="email" /></td>
			</tr>
			<tr>
				<td>MobileNo:</td>
				<td><form:input path="mobileno" /></td>
			</tr>
			<tr>
				<td>Date</td>
				<td><form:input path="date" /></td>
			</tr>

			<tr>
				<td></td>
				<td><input type="submit" value=" Save" /></td>
			</tr>
		</table>

	</form:form>
</body>
</html>