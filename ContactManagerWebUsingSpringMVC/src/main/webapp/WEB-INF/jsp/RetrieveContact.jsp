<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
   <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>    
  
<h1>Contacts List</h1>  
<table border="2" width="70%" cellpadding="2">  
<tr><th>UserId</th><th>ContactFirstName</th><th>ContactLastName</th><th>Email</th><th>MobileNo</th><th>Date</th></tr>  
   <c:forEach var="UserContacts" items="${list}">   
   <tr>  
   <td>${UserContacts.userId}</td>  
   <td>${UserContacts.contactFirstName}</td>  
   <td>${UserContacts.contactLastName}</td>  
   <td>${UserContacts.email}</td>
   <td>${UserContacts.mobileno}</td>
   <td>${UserContacts.date}</td>  
   </tr>  
   </c:forEach>  
   </table>  
   <br/>  
   <a href="contactform">Add New Contact</a>  
   </body>
</html>