package com.gl.app.spring.ContactManagerUsingSpring;

import java.util.*;
import java.util.Map.Entry;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {

		ApplicationContext cont = new AnnotationConfigApplicationContext(ContactUser.class);
		User eobj = (User) cont.getBean("userobj");
		// System.out.println("User Id is "+eobj.getUserId());
		// System.out.println("User name is "+eobj.getUsername());
		Contact d = (Contact) cont.getBean("contactobj");
		// System.out.println("Contact First name is "+d.getContactFirstName());
		System.out.println("*********************************************");
		System.out.println("*********************************************");
		System.out.println("....WELCOME TO CONTACT MANAGEMENT SYSTEM.....");
		System.out.println("*********************************************");
		System.out.println("*********************************************");
		System.out.println("User Contact Details Using Spring");
		System.out.println(".............................................");
		Map<User, Contact> mapcontacts = d.getContactsMap();
		Set<Map.Entry<User, Contact>> mapentry = mapcontacts.entrySet();
		for (Entry<User, Contact> oneentry : mapentry) {
			User uobj = oneentry.getKey();
			System.out.println("UserId: " + uobj.getUserId());
			System.out.println(".............................................");
			Contact cobj = oneentry.getValue();
			System.out.println("Contact First Name: " + cobj.getContactFirstName());
			System.out.println("Contact Last Name: " + cobj.getContactLastName());
			System.out.println("Email: " + cobj.getEmail());
			System.out.println("Mobile No:" + cobj.getMobileNo());
			System.out.println("Date: " + cobj.getDate());
			System.out.println(".............................................");
		}

	}

}
