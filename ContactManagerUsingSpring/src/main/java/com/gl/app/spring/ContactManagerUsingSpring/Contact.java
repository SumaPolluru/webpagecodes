package com.gl.app.spring.ContactManagerUsingSpring;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Contact {
	private String contactFirstName;
	private String contactLastName;
	private String email;
	private String mobileNo;
	private String date;

	public String getContactFirstName() {
		return contactFirstName;
	}

	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}

	public String getContactLastName() {
		return contactLastName;
	}

	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Map<User, Contact> getContactsMap() {
		return contactsMap;
	}

	public void setContactsMap(Map<User, Contact> contactsMap) {
		this.contactsMap = contactsMap;
	}

	@Autowired
	@Qualifier("elist")
	private Map<User, Contact> contactsMap;


	@Override
	public int hashCode() {
		return Objects.hash(contactFirstName, contactLastName, mobileNo);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contact other = (Contact) obj;
		return Objects.equals(contactFirstName, other.contactFirstName)
				&& Objects.equals(contactLastName, other.contactLastName) && Objects.equals(mobileNo, other.mobileNo);
	}

}
