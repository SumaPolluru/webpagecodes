package com.gl.app.spring.ContactManagerUsingSpring;
import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ContactUser {
	@Bean(name="userobj")
	public User getUser()
	{
		User e=new User();
		e.setUserId(10);
		e.setUsername("Ramya");
		e.setPassword("Ramya12");	
		return e;

	}
	@Bean(name="userobj1")
	public User getUser1()
	{
		User e1=new User();
		e1.setUserId(20);
		e1.setUsername("Chintu");
		e1.setPassword("Rash12");
		return e1;
	}
	@Bean(name="contactobj")
	public Contact getContact()
	{
		Contact d1=new Contact();
		d1.setContactFirstName("Ravi");
		d1.setContactLastName("Varma");
		d1.setMobileNo("8008970246");
		return d1;
	}
	@Bean(name = "elist")
	public Map<User,Contact> getContactsMap() {
		Map<User, Contact> contactsmap = new HashMap<User, Contact>();
		User u=new User();
		u.setUserId(110);
		u.setUsername("Suma");
		u.setPassword("Chintu12");
		
		Contact c = new Contact();
		c.setContactFirstName("Suma");
		c.setContactLastName("Sree");
		c.setEmail("Sree@gmail.com");
		c.setMobileNo("9381749152");
		c.setDate("2022-10-06");
	
		User u1=new User();
		u1.setUserId(105);
		u1.setUsername("Priya");
		u1.setUsername("Priya12");
		
		Contact c1 = new Contact();
		c1.setContactFirstName("Gaye");
		c1.setContactLastName("Reddy");
		c1.setEmail("Gaye@gmail.com");
		c1.setMobileNo("9987654331");
		c1.setDate("2022-10-05");
		contactsmap.put(u,c);
		contactsmap.put(u1,c1);
		
		return contactsmap;

	}
}
