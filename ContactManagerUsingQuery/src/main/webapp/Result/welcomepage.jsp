<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Welcome</title>
</head>
<style>
body {
	font-family: Calibri;
	background-color: #c7ddcc;
}

a {
	font-size: 25px;
}
</style>
<body>
	<h1>WELCOME TO CONTACT MANAGEMENT SYSTEM</h1>
	<table>
		<tr>
			<td><a href="InsertContact">INSERT CONTACT</a></td>
		</tr>
		<tr>
			<td><a href="RetrieveDetails">RETRIEVE CONTACT DETAILS</a></td>
		</tr>
		<tr>
			<td><a href="GetById">UPDATE CONTACT</a></td>
		</tr>
		<tr>
			<td><a href="deleteById">DELETE CONTACT</a></td>
		</tr>
	</table>
	<H3>ADDRESS OPERATIONS</H3>
	<table>
		<tr>
			<td><a href="InsertAddress">INSERT ADDRESS</a></td>
		</tr>
		<tr>
			<td><a href="AddressDetails">RETRIEVE ADDRESS DETAILS</a></td>
		</tr>
		<tr>
			<td><a href="UpdateById">UPDATE ADDRESS</a></td>
		</tr>
		<tr>
			<td><a href="AddressdeleteById">DELETE ADDRESS</a></td>
		</tr>
	</table>

	<table>
		<tr>
			<td><a href="logout"><font color=Blue size=5>Logout</font></a></td>
		</tr>
	</table>
</body>
</html>