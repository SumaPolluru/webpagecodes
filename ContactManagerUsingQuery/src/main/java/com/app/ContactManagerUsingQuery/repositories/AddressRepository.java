package com.app.ContactManagerUsingQuery.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.ContactManagerUsingQuery.model.Address;

	@Repository
	public interface AddressRepository extends  JpaRepository<Address,Integer>
	{



	}
