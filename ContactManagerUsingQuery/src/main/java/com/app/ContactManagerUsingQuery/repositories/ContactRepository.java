
package com.app.ContactManagerUsingQuery.repositories;

import java.util.List;
import com.app.ContactManagerUsingQuery.model.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface ContactRepository extends JpaRepository<Contact, Integer> {

	public List<Contact> findByOrderByFirstNameAsc();

	@Query("select con.firstName,con.lastName from Contact con order by lastName ASC")
	public List<String> findAllOrderByLastNameAsc();
	
	
}
