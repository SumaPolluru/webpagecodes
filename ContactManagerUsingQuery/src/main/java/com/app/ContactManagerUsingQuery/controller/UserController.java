package com.app.ContactManagerUsingQuery.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.app.ContactManagerUsingQuery.model.Contact;
import com.app.ContactManagerUsingQuery.model.User;
import com.app.ContactManagerUsingQuery.repositories.UserRepository;

@Controller
public class UserController {
	@Autowired
	UserRepository usrep;

	@RequestMapping("/CountData")
	public String getQuery() {
		List<Long> clist = usrep.getConList(2);
		for (Long e1 : clist) {
			System.out.println("Count " + e1);
		}
		return "success";
	}

	@RequestMapping("/MobileNumberQuery")
	public String getCon() {
		List<Contact> clist = usrep.getContacts();
		if (clist != null) {
			for (Contact e1 : clist) {
				System.out.println("Contact Id " + e1.getContactId());
				System.out.println("Contact First Name: " + e1.getFirstName());
				System.out.println("Contact Last Name: " + e1.getLastName());
				System.out.println("Email Id: " + e1.getEmailId());
				System.out.println("Mobile Number :" + e1.getMobileNumber());
			}

		}
		return "success";
	}

	@RequestMapping("/AjayQuery")
	public String getAjayDetails() {
		List<Contact> clist = usrep.getAjayData();
		if (clist != null) {
			for (Contact e1 : clist) {
				System.out.println("Contact Id " + e1.getContactId());
				System.out.println("Contact First Name: " + e1.getFirstName());
				System.out.println("Contact Last Name: " + e1.getLastName());
				System.out.println("Email Id: " + e1.getEmailId());
				;
				System.out.println("Mobile Number :" + e1.getMobileNumber());
			}

		}
		return "success";
	}

	@RequestMapping("/AjayDelhiQuery")
	public String getAjayD() {
		List<Contact> clist = usrep.getAjayDelhiData();
		if (clist != null) {
			for (Contact e1 : clist) {
				System.out.println("Contact Id " + e1.getContactId());
				System.out.println("Contact First Name: " + e1.getFirstName());
				System.out.println("Contact Last Name: " + e1.getLastName());
				System.out.println("Email Id: " + e1.getEmailId());
				System.out.println("Mobile Number :" + e1.getMobileNumber());
			}

		}
		return "success";
	}

	@RequestMapping("/deleteByuserId") // Contacts also automatically deleted
	public ModelAndView deleteByUserId() {

		return new ModelAndView("deleteUserForm", "uobj", new User());
	}

	@RequestMapping("/deleteUser")
	public ModelAndView deleteUser(@ModelAttribute("uobj") User user) {
		usrep.deleteById(user.getUserId());
		return new ModelAndView("success");
	}

	/*
	 * @RequestMapping("/deleteUserContacts") //Tried using query but not supported
	 * public String getdeletionStatus() { List<Contact>clist =
	 * usrep.getDeleteId(2); if (clist == null) {
	 * System.out.println("Successfully deleted"); } return "success"; }
	 */

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(String error, String logout) {
		System.out.println("Coming here ");
		if (error != null)
			return new ModelAndView("login", "errorM	sg", "Your username and password are invalid.");

		if (logout != null)
			return new ModelAndView("login", "msg", "You have been logged out successfully.");
		return new ModelAndView("login");
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpServletRequest request) {
		HttpSession httpSession = request.getSession();
		httpSession.invalidate();
		return "redirect:/login";
	}
}
