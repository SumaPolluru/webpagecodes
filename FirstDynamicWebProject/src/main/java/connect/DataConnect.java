package connect;
import java.sql.*;
	public class DataConnect 
	{
		//Using SingleTon Design Pattern
		private static Connection con;
		// This interface is responsible for establishing database connection
		private DataConnect()
		{
			try 
			{
				Class.forName("com.mysql.cj.jdbc.Driver");
				con = DriverManager.getConnection("jdbc:mysql://localhost:3306/contactmanagementsystem", "root", "@Chintu12345");
				System.out.println("Connection established");
			}
			catch (Exception e) 
			{
				System.out.println("Exception is " + e.getMessage());
			}
		}

		public static Connection getConnect()
		{
			DataConnect d1 = new DataConnect();
			return con;
		}

}
