package myservlet;

import java.io.IOException;

import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import connect.DataConnect;
/**
 * Servlet implementation class EmployeeServlet
 */
@WebServlet("/EmployeeServlet")
public class EmployeeServlet extends HttpServlet {
/*
 * By Default Any Servlet will handle request of Get type
 * if u want to handle request of post type 
 * that has to be written in doPost method.
 */
	private Connection con;
	public void doPost(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException
	{
		con=DataConnect.getConnect();
		String employeecode=req.getParameter("tempcode");
		String employeename=req.getParameter("tempname");
		double salary=Double.parseDouble(req.getParameter("salary"));
		PrintWriter  out=res.getWriter();
		out.println("Employee code is "+employeecode);
		out.println("\nEmployee name is "+employeename);
		out.println("\nSalary is "+salary);			
	}
}
