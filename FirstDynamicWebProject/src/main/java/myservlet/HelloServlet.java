package myservlet;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class HelloServlet extends HttpServlet {
	public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		PrintWriter out = res.getWriter();// We are retrieving reference of
		// Printwriter for response object.
		out.println("Welcome to First Servlet Application");
	}

}
