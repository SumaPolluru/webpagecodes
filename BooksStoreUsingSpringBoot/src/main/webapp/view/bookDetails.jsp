<!DOCTYPE html >
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="sat, 01 Dec 2001 00:00:00 GMT">
<title>Welcome</title>
<link href="static/css/bootstrap.min.css" rel="stylesheet">
<link href="static/css/style.css" rel="stylesheet">

</head>
<style>
th {
	background-color: black;
	color:white;
	text-align: center;
	text-size:5px;
}
</style>
<body>
	<div role="navigation">
		<div class="navbar navbar-inverse">
			<a href="/userHome" class="navbar-brand"><font color=white
				size=4px>User</font></a>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="/show-books"><font color=white size=4px>Show
								Books</font></a></li>
					<li><a href="/logout"><font color=white size=4px>Logout</font></a></li>
				</ul>
			</div>
		</div>
	</div>
	<c:choose>
		<c:when test="${userobj=='HOME' }">
			<div class="container" id="homediv">
				<div class="jumbotron text-center">
					<h2>
						<font size=8px>Welcome to User Operations Page</font>
					</h2>
					<h3>These Operations are Performed By User</h3>
				</div>
			</div>
		</c:when>
		<c:when test="${bookobj=='ADD_BOOK' }">
			<div class="container text-center">
				<h3>Add Book</h3>
				<hr>
				<form class="form-horizontal" method="POST" action="save-book">
					<input type="hidden" name="bookId" value="${book.bookId }" />
					<div class="form-group">
						<label class="control-label col-md-3"> Book Name</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="bookName"
								value="${book.bookName}" placeholder="BookName" required/>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">Author Name</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="authorName"
								value="${book.authorName}" placeholder="Author Name" required/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Description</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="description"
								value="${book.description}" placeholder="Description" required/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Genre</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="genre"
								value="${book.genre}" placeholder="Genre" required/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Price</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="price"
								value="${book.price}" placeholder="Price" required/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">No.Of Copies</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="noOfCopies"
								value="${book.noOfCopies}" placeholder="No Of Copies" required/>
						</div>
					</div>
					<div class="form-group ">
						<input type="submit" class="btn btn-primary" value="Add Book" />
					</div>
				</form>
			</div>
		</c:when>
		<c:when test="${bookobj=='BOOKS' }">
			<div class="container text-center" id="tasksDiv">
				<h3>BOOKS</h3>
				<hr>
				<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>SNo.</th>
								<th>Book Id</th>
								<th>Book Name</th>
								<th>Author Name</th>
								<th>Description</th>
								<th>Publication</th>
								<th>Price</th>
								<th>Total No.Of Copies</th>
								<th>Delete</th>
								<th>Edit</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="book" items="${books}" varStatus="status">
								<tr>
									<td>${status.index + 1}</td>
									<td>${book.bookId}</td>
									<td>${book.bookName}</td>
									<td>${book.authorName}</td>
									<td>${book.description}</td>
									<td>${book.genre}</td>
									<td>${book.price}</td>
									<td>${book.noOfCopies}</td>
									<td><a href="/delete-book?bookId=${book.bookId }"><span
											class="glyphicon glyphicon-trash"></span></a></td>
									<td><a href="/edit-book?bookId=${book.bookId }"><span
											class="glyphicon glyphicon-pencil"></span></a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<center>

				<a href="/addBook"><font size=5px>Add Book</font></a>

			</center>
		</c:when>
		<c:when test="${bookobj=='BOOK-UPDATE' }">
			<div class="container text-center">
				<h3>Update Book</h3>
				<hr>
				<form class="form-horizontal" method="POST" action="updateBook">
					<input type="hidden" name="bookId" value="${book.bookId }" />
					<div class="form-group">
						<label class="control-label col-md-3">Book Name</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="bookName"
								value="${book.bookName }" required/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Author Name</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="authorName"
								value="${book.authorName }" required/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Description</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="description"
								value="${book.description}" required/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Genre</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="genre"
								value="${book.genre}" required/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Price</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="price"
								value="${book.price}" required/>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">Total No.Of Copies</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="noOfCopies"
								value="${book.noOfCopies}" required/>
						</div>
					</div>
					<div class="form-group ">
						<input type="submit" class="btn btn-primary" value="Update" />
					</div>
				</form>
			</div>
		</c:when>
	</c:choose>
	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="static/js/jquery-1.11.1.min.js"></script>
	<script src="static/js/bootstrap.min.js"></script>
</body>
</html>