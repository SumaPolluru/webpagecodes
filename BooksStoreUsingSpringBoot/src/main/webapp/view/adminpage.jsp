<!DOCTYPE html >
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="sat, 01 Dec 2001 00:00:00 GMT">
<title>Admin|Home</title>
<link href="static/css/bootstrap.min.css" rel="stylesheet">
<link href="static/css/style.css" rel="stylesheet">


</head>
<body>
	<div role="navigation">
		<div class="navbar navbar-inverse">
			<a href="/home" class="navbar-brand"><font color=white size=4px>Admin-Page</font></a>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="/adminLogin"><font color=white size=4px>Login</font></a></li>
					<li><a href="/registeration"><font color=white size=4px>New Registration</font></a></li>
					<li><a href="/logout"><font color=white size=4px>Logout</font></a></li>
				</ul>
			</div>
		</div>
	</div>

	<c:choose>
		<c:when test="${adminobj=='ADMIN_HOME' }">
			<div class="container" id="homediv">
				<div class="jumbotron text-center">
					<h2>
						<font size=8px>Welcome to Magic Of Books Store </font>
					</h2>
					<h3>Need Credentials for admin Operations</h3>
				</div>
			</div>

		</c:when>

		<c:when test="${adminobj=='ADMIN_REGISTER' }">
			<div class="container text-center">
				<h3>New Registration</h3>
				<hr>
				<form class="form-horizontal" method="POST" action="save-admin">
					<c:if test="${not empty error }">
						<div class="alert alert-danger">
							<c:out value="${error }"></c:out>
						</div>
					</c:if>
					<input type="hidden" name="adminId" value="${admin.adminId }" />
					<div class="form-group">
						<label class="control-label col-md-3">Admin Name</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="adminName"
								value="${admin.adminName }" placeholder="Email" required />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">Password</label>
						<div class="col-md-7">
							<input type="password" class="form-control" name="adminPassword"
								value="${admin.adminPassword }" placeholder="Password" required/>
						</div>
					</div>
					<div class="form-group ">
						<input type="submit" class="btn btn-primary" value="Register" />
					</div>
				</form>
			</div>
		</c:when>
		<c:when test="${adminobj=='ADMIN_LOGIN' }">
			<div class="container text-center">
				<h3>Admin Login</h3>
				<hr>
				<form class="form-horizontal" method="POST" action="/login-admin">
					<c:if test="${not empty error }">
						<div class="alert alert-danger">
							<c:out value="${error }"></c:out>
						</div>
					</c:if>
					<div class="form-group">
						<label class="control-label col-md-3">Admin Name</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="adminName"
								value="${admin.adminName}" placeholder="Email" required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Password</label>
						<div class="col-md-7">
							<input type="password" class="form-control" name="adminPassword"
								value="${admin.adminPassword }" placeholder="Password" required/>
						</div>
					</div>
					<div class="form-group ">
						<input type="submit" class="btn btn-primary" value="Login" />
					</div>
				</form>
			</div>
		</c:when>
	</c:choose>
	
	<script src="static/js/jquery-1.11.1.min.js"></script>
	<script src="static/js/bootstrap.min.js"></script>
</body>
</html>