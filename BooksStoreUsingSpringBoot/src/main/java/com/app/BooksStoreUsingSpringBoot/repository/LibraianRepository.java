package com.app.BooksStoreUsingSpringBoot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.app.BooksStoreUsingSpringBoot.model.Libraian;
@Repository
public interface LibraianRepository extends JpaRepository<Libraian, String>  {

	//Queries
		public Libraian findByLibraianIdAndPassword(String libraianId, String password);
		
		public Libraian findByLibraianId(String libraianId);
		
		@Query("Select user.libraianId from Libraian user")
		public List<String> getUser();
}
