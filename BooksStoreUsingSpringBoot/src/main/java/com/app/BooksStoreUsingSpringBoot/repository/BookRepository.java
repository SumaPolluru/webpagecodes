package com.app.BooksStoreUsingSpringBoot.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.app.BooksStoreUsingSpringBoot.model.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Integer> 
{
	@Query("Select book.bookId from Book book")
	public List<Integer> getBook();

}
