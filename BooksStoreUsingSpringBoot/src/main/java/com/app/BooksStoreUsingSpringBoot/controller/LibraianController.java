package com.app.BooksStoreUsingSpringBoot.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.app.BooksStoreUsingSpringBoot.model.Libraian;
import com.app.BooksStoreUsingSpringBoot.repository.LibraianRepository;
import com.app.BooksStoreUsingSpringBoot.service.LibraianService;

@Controller
public class LibraianController {

	@Autowired
	private LibraianRepository libraianRepository;

	@Autowired
	LibraianService userService;
	
	@RequestMapping("/welcome")
	public String Welcome(HttpServletRequest request) 
	{
		request.setAttribute("mode", "MODE_HOME");
		return "welcomepage";
	}

	@RequestMapping("/delete-user")
	public String deleteUser(@RequestParam String libraianId, HttpServletRequest request) {
		userService.deleteMyUser(libraianId);
		request.setAttribute("users", userService.showAllUsers());
		request.setAttribute("mode", "ALL_USERS");
		return "adminWelcomePage";
	}

	@RequestMapping("/edit-user")
	public String editUser(@RequestParam String libraianId, HttpServletRequest request) {
		request.setAttribute("user", userService.editUser(libraianId));
		request.setAttribute("mode", "MODE_UPDATE");
		return "adminWelcomePage";
	}

	@RequestMapping("/saveUpdation")
	public ModelAndView saveUpdates(@ModelAttribute("mode") Libraian user) 
	{
		libraianRepository.save(user);
		return new ModelAndView("adminWelcomePage");
	}
	
	@RequestMapping("/register")   //User Register
	public String registration(HttpServletRequest request)
	{
		request.setAttribute("mode", "MODE_REGISTER");
		return "welcomepage";
	}

	@PostMapping("/save-user")   //Checking Register Credentials,The User is Present or Not
	public String registerUser(@ModelAttribute Libraian user, BindingResult bindingResult, HttpServletRequest request)
	{
	if (userService.findByLibraianId(user.getLibraianId()) != null) 
	{
		request.setAttribute("error", "UserId already Exist");
		request.setAttribute("mode", "MODE_REGISTER");
		return "welcomepage";
	}
	else
	{
		userService.saveMyUser(user);
		request.setAttribute("mode", "MODE_HOME");
		return "welcomepage";
	}
}

	@RequestMapping("/login")
	public String login(HttpServletRequest request) 
	{
		request.setAttribute("mode", "MODE_LOGIN");
		return "welcomepage";
	}

	@RequestMapping("/login-user")  //Checking Login Credentials
	public String loginUser(@ModelAttribute Libraian user, HttpServletRequest request) 
	{
		if (userService.findByUserIdAndPassword(user.getLibraianId(), user.getPassword()) != null)
		{
			return "bookDetails";
		}
		else
		{
			request.setAttribute("error", "Invalid Username or Password");
			request.setAttribute("mode", "MODE_LOGIN");
			return "welcomepage";

		}
	}

	//Logout
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpServletRequest request) 
	{
		HttpSession httpSession = request.getSession();
		httpSession.invalidate();
		return "redirect:/";
	}

	
	@GetMapping("/saveuser")  //Saving Registration Data
	public String saveUser(@RequestParam String libraianId, @RequestParam String firstname, @RequestParam String lastname,
			@RequestParam String mobileNo, @RequestParam String address, @RequestParam String password)
	{
		Libraian user = new Libraian(libraianId, firstname, lastname, mobileNo, address, password);
		userService.saveMyUser(user);
		return "User Saved";
	}
	
	//User Home
	@RequestMapping("/userHome")
	public String Admin(HttpServletRequest request)
	{
		request.setAttribute("userobj", "HOME");
		return "bookDetails";
	}
}