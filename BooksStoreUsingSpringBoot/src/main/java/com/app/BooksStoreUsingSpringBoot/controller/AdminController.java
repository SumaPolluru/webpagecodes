package com.app.BooksStoreUsingSpringBoot.controller;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.app.BooksStoreUsingSpringBoot.model.Admin;
import com.app.BooksStoreUsingSpringBoot.service.AdminService;
import com.app.BooksStoreUsingSpringBoot.service.LibraianService;

@Controller
public class AdminController
{

	@Autowired
	AdminService adminService;

	@Autowired
	LibraianService userService;
	
	//Start 
	@RequestMapping("/")
	public String Welcome() 
	{
		return "magicOfBooks";
	}


	@RequestMapping("/home")
	public String Welcome(HttpServletRequest request)
	{
		request.setAttribute("adminobj", "ADMIN_HOME");
		return "adminpage";
	}
	
	@RequestMapping("/registeration")
	public String registration(HttpServletRequest request)
	{
		request.setAttribute("adminobj", "ADMIN_REGISTER");
		return "adminpage";
	}

	@RequestMapping("/save-admin") //Registration,checking that admin is present or not
	public String registerUser(@ModelAttribute Admin admin, BindingResult bindingResult, HttpServletRequest request)
	{
		if (adminService.findByAdminName(admin.getAdminName()) != null) 
		{
			request.setAttribute("error", "AdminName already Exist");
			request.setAttribute("adminobj", "ADMIN_REGISTER");
			return "adminpage";
		}
		else 
		{
		adminService.saveAdmin(admin);
		request.setAttribute("adminobj", "ADMIN_HOME");
		return "adminpage";
  	    }
	}

	@RequestMapping("/adminLogin")
	public String login(HttpServletRequest request)
	{
		request.setAttribute("adminobj", "ADMIN_LOGIN");
		return "adminpage";
	}

	@RequestMapping("/login-admin")  //Checking Login Credentials
	public String loginUser(@ModelAttribute Admin admin, HttpServletRequest request) 
	{
		if (adminService.findByAdminNameAndAdminPassword(admin.getAdminName(), admin.getAdminPassword()) != null)
		{
			return "adminWelcomePage";
		}
		else
		{
			request.setAttribute("error", "Invalid AdminName or Password");
			request.setAttribute("adminobj", "ADMIN_LOGIN");
			return "adminpage";

		}
	}

	@RequestMapping("/saveadmin")  //Here, Registration data should store
	public String saveUser(@RequestParam String adminName, @RequestParam String adminPassword) 
	{
			Admin admin = new Admin(adminName,adminPassword);
			adminService.saveAdmin(admin);
			return "Admin Saved";
	}
	
	@RequestMapping("/adminHome")
	public String home(@ModelAttribute Admin admin, BindingResult bindingResult, HttpServletRequest request) 
	{
		adminService.saveAdmin(admin);
		request.setAttribute("adminobj", "HOME");
		return "adminWelcomePage";
	}
	
	@GetMapping("/show-users") //CRUD Operations on User
	public String showAllUsers(HttpServletRequest request) 
	{
		request.setAttribute("users", userService.showAllUsers());
		request.setAttribute("mode", "ALL_USERS");
		return "adminWelcomePage";
	}

}
