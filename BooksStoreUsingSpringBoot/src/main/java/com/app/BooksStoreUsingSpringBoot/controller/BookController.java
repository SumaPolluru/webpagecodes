package com.app.BooksStoreUsingSpringBoot.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.app.BooksStoreUsingSpringBoot.model.Book;
import com.app.BooksStoreUsingSpringBoot.repository.BookRepository;
import com.app.BooksStoreUsingSpringBoot.repository.LibraianRepository;
import com.app.BooksStoreUsingSpringBoot.service.BookService;


@Controller
public class BookController
{

	@Autowired
	BookService bookService;

	@Autowired
	private BookRepository bookRepository;
	
	@Autowired
	private LibraianRepository userRepository;
	
	@RequestMapping("/addBook") //Insert Book
	public String addMenuDetails(HttpServletRequest request) 
	{
		request.setAttribute("bookobj", "ADD_BOOK");
		return "bookDetails";
	}

	@PostMapping("/save-book")
	public String menuDetails(@ModelAttribute Book book, BindingResult bindingResult, HttpServletRequest request) 
	{
		bookService.saveBook(book);
		request.setAttribute("bookobj", "ADMIN_HOME");
		return "bookSaved";
	}

	@GetMapping("/saveBook")
	public String saveMenuDate(@RequestParam String bookName, @RequestParam String authorName,
			@RequestParam String description, @RequestParam String genre, @RequestParam double price,
			@RequestParam int noOfBooks) 
	{
		Book book = new Book(bookName, authorName, description, genre, price, noOfBooks);
		bookService.saveBook(book);
		return "bookSaved";
	}

	@GetMapping("/show-books")  //Retrieve Books
	public String showBookData(HttpServletRequest request)
	{
		request.setAttribute("books", bookService.showBooks());
		request.setAttribute("bookobj", "BOOKS");
		return "bookDetails";
	}

	@RequestMapping("/delete-book") //Delete Books
	public String deleteBook(@RequestParam int bookId, HttpServletRequest request)
	{
		bookService.deleteBookById(bookId);
		request.setAttribute("books", bookService.showBooks());
		request.setAttribute("bookobj", "BOOKS");
		return "bookDetails";
	}

	@RequestMapping("/edit-book") //Update Books
	public String editBook(@RequestParam int bookId, HttpServletRequest request) 
	{
		request.setAttribute("book", bookService.editBookDetails(bookId));
		request.setAttribute("bookobj", "BOOK-UPDATE");
		return "bookDetails";
	}

	@RequestMapping("updateBook")
	public ModelAndView saveUpdate(@ModelAttribute("bookobj") Book book) 
	{
		bookRepository.save(book);
		return new ModelAndView("bookDetails");
	}

	@RequestMapping({ "/bookDetails" })  //Book Details for UserData without CRUD Operations
	public ModelAndView getDetails(HttpServletRequest request)
	{
		List<Book> booklist = bookService.showBooks();
		request.setAttribute("bookobj", "SEARCHDATA");
		return new ModelAndView("bookDetails", "bookdata", booklist);
	}
	
}