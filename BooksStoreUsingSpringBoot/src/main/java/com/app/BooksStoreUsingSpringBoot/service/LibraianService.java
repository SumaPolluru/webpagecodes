package com.app.BooksStoreUsingSpringBoot.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

import com.app.BooksStoreUsingSpringBoot.model.Libraian;
import com.app.BooksStoreUsingSpringBoot.repository.LibraianRepository;

@Service
@Transactional
public class LibraianService {
	
	private LibraianRepository libraianRepository;
	
	public LibraianService(LibraianRepository libraianRepository) {
		this.libraianRepository=libraianRepository;
	}
	
	//Business Logic for CRUD Operations On User
	public void saveMyUser(Libraian user ) {
		libraianRepository.save(user);
	}
	
	public Libraian findByUserIdAndPassword(String libraianId, String password) {
		return libraianRepository.findByLibraianIdAndPassword(libraianId, password);
	}
	public List<Libraian> showAllUsers(){
		List<Libraian> users = new ArrayList<Libraian>();
		for(Libraian user : libraianRepository.findAll()) {
			users.add(user);
		}
		
		return users;
	}
	
	public void deleteMyUser(String libraianId) {
		libraianRepository.deleteById(libraianId);
	}
	
	public Libraian editUser(String libraianId) {
		Optional<Libraian> uobj=libraianRepository.findById(libraianId);
		return uobj.get();
	}

	public Libraian findByLibraianId(String libraianId) {
		return libraianRepository.findByLibraianId(libraianId);
	}
}
