package com.app.BooksStoreUsingSpringBoot.service;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.BooksStoreUsingSpringBoot.model.Admin;
import com.app.BooksStoreUsingSpringBoot.repository.AdminRepository;

@Service
@Transactional
public class AdminService 
{

	@Autowired
	private AdminRepository adminRepository;

	public AdminService(AdminRepository adminRepository)
	{
		this.adminRepository = adminRepository;
	}

	public Admin saveAdmin(Admin admin)
	{
		return adminRepository.save(admin);
	}

	public Admin findByAdminNameAndAdminPassword(String adminName, String adminPassword) 
	{
		return adminRepository.findByAdminNameAndAdminPassword(adminName, adminPassword);
	}

	// Register credentials checking
	public Admin findByAdminName(String adminName) 
	{
		return adminRepository.findByAdminName(adminName);
	}


}
