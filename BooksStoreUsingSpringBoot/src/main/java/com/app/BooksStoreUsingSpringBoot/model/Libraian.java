package com.app.BooksStoreUsingSpringBoot.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Libraian")
public class Libraian {

	@Id
	private String libraianId;
	private String firstName;
	private String lastName;
	private String mobileNo;
	private String address;
	private String password;

	public Libraian()
	{

	}

	public Libraian(String libraianId, String firstName, String lastName, String mobileNo, String address, String password) 
	{
		super();
		this.libraianId = libraianId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.mobileNo = mobileNo;
		this.address = address;
		this.password = password;
	}


	public void setLibraianId(String libraianId)
	{
		this.libraianId = libraianId;
	}
	public String getLibraianId()
	{
		return libraianId;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public String getMobileNo()
	{
		return mobileNo;
	}

	public void setMobileNo(String mobileNo)
	{
		this.mobileNo = mobileNo;
	}

	public String getAddress() 
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	@Override
	public String toString() {
		return " \n LibraianId:" + libraianId + "\n Mobile No:" + mobileNo + "\n Address:" + address ;
	}
}	