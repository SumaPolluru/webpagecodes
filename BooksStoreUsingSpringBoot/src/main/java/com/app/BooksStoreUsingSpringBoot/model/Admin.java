package com.app.BooksStoreUsingSpringBoot.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Admin")
public class Admin
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int adminId;
	private String adminName;
	private String adminPassword;

	public Admin()
	{

	}

	public Admin(String adminName, String adminPassword) 
	{
		super();
		this.adminName = adminName;
		this.adminPassword = adminPassword;
	}


	public Admin(int adminId, String adminName, String adminPassword) 
	{
		super();
		this.adminId = adminId;
		this.adminName = adminName;
		this.adminPassword = adminPassword;
	}

	public int getAdminId() 
	{
		return adminId;
	}

	public void setAdminId(int adminId) 
	{
		this.adminId = adminId;
	}

	public String getAdminName()
	{
		return adminName;
	}

	public void setAdminName(String adminName) 
	{
		this.adminName = adminName;
	}

	public String getAdminPassword()
	{
		return adminPassword;
	}

	public void setAdminPassword(String adminPassword) 
	{
		this.adminPassword = adminPassword;
	}

	@Override
	public String toString()
	{
		return "Admin [adminName=" + adminName + ", adminPassword=" + adminPassword + "]";
	}

}
