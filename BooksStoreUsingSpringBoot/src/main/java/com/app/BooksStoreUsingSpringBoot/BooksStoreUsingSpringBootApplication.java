package com.app.BooksStoreUsingSpringBoot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BooksStoreUsingSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(BooksStoreUsingSpringBootApplication.class, args);
	}

}
