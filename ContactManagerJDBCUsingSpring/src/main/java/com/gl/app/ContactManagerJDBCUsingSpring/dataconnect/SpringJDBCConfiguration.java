package com.gl.app.ContactManagerJDBCUsingSpring.dataconnect;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import com.gl.app.ContactManagerJDBCUsingSpring.service.UserContactService;
import com.gl.app.ContactManagerJDBCUsingSpring.dao.UserContactDAOImpl;

@Configuration
public class SpringJDBCConfiguration {
	DriverManagerDataSource datasource;
	JdbcTemplate jdbcTemplate;
	UserContactDAOImpl pdaoimpl;

	@Bean
	public DataSource dataSource() {

		datasource = new DriverManagerDataSource();
		datasource.setDriverClassName("com.mysql.cj.jdbc.Driver");
		datasource.setUrl("jdbc:mysql://localhost:3306/contactmanagementsystem");
		datasource.setUsername("root");
		datasource.setPassword("@Chintu12345");
		return datasource;

	}

	@Bean
	public JdbcTemplate jdbcTemplate() {
		jdbcTemplate = new JdbcTemplate();
		jdbcTemplate.setDataSource(datasource);
		return jdbcTemplate;

	}

	@Bean
	public UserContactDAOImpl userContactDAO() {
		pdaoimpl = new UserContactDAOImpl();
		pdaoimpl.setJdbcTemplate(jdbcTemplate);
		return pdaoimpl;
	}

	@Bean
	public UserContactService userContactService() {
		UserContactService pservice = new UserContactService();
		pservice.setPdaoimpl(pdaoimpl);
		return pservice;
	}
}