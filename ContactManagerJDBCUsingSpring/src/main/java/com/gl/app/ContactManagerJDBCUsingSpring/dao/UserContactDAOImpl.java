package com.gl.app.ContactManagerJDBCUsingSpring.dao;
	import org.springframework.jdbc.core.JdbcTemplate;

import com.gl.app.ContactManagerJDBCUsingSpring.pojo.UserContacts;


	public class UserContactDAOImpl implements UserContactDAO{
		public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
			this.jdbcTemplate = jdbcTemplate;
		}

		private JdbcTemplate jdbcTemplate;

		@Override
		public void insert(UserContacts u) {
			String query = "insert into usercontacts values(?,?,?,?,?,?)";
			jdbcTemplate.update(query, u.getUserId(), u.getContactFirstName(), u.getContactLastName(),u.getEmail(),u.getMobileno(),u.getDate());
			System.out.println("Inserted succesfully");

		}

		@Override
		public UserContacts getUserContacts() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void delete(UserContacts u) {
			String query = "delete from usercontacts where userId=?";
			jdbcTemplate.update(query, u.getUserId());
			System.out.println("Deleted succesfully");

			
		}

	}