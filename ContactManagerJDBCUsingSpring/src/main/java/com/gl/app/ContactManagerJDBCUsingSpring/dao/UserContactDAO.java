package com.gl.app.ContactManagerJDBCUsingSpring.dao;

import com.gl.app.ContactManagerJDBCUsingSpring.pojo.UserContacts;

public interface UserContactDAO {
	public void insert(UserContacts u);

	public void delete(UserContacts u);

	public UserContacts getUserContacts();
}
