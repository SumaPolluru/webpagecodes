package com.gl.app.ContactManagerJDBCUsingSpring.service;

import java.util.Scanner;
import com.gl.app.ContactManagerJDBCUsingSpring.dao.UserContactDAO;
import com.gl.app.ContactManagerJDBCUsingSpring.pojo.UserContacts;

public class UserContactService {
	private Scanner sc;
	private UserContacts u;
	private UserContactDAO pdaoimpl;

	public void setPdaoimpl(UserContactDAO pdaoimpl) {
		this.pdaoimpl = pdaoimpl;
	}

	public UserContactService() {
		sc = new Scanner(System.in);
	}

	public void insertData() {
		u = new UserContacts();
		System.out.println("...........................");
		System.out.println("Enter User Id ");
		u.setUserId(sc.nextInt());
		System.out.println("Enter Contact First Name");
		u.setContactFirstName(sc.next());
		System.out.println("Enter Contact Last Name");
		u.setContactLastName(sc.next());
		System.out.println("Enter Email");
		u.setEmail(sc.next());
		System.out.println("Enter Mobile No");
		u.setMobileno(sc.nextLong());
		System.out.println("Enter Date[YYYY-MM-DD]");
		u.setDate(sc.next());
		pdaoimpl.insert(u);
	}
	public void deleteData() {
		u = new UserContacts();
		System.out.println("Enter User Id, Which u want to delete from the database");
		u.setUserId(sc.nextInt());
		pdaoimpl.delete(u);
	}
}
