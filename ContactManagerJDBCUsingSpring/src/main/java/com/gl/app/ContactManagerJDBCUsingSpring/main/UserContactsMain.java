package com.gl.app.ContactManagerJDBCUsingSpring.main;

import java.util.Scanner;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.gl.app.ContactManagerJDBCUsingSpring.dataconnect.SpringJDBCConfiguration;
import com.gl.app.ContactManagerJDBCUsingSpring.service.UserContactService;

public class UserContactsMain {
	/*
	 * static Scanner sc;
	 * 
	 * UserContactsMain() { sc = new Scanner(System.in); }
	 */

	public static void main(String args[]) {
		ApplicationContext cont = new AnnotationConfigApplicationContext(SpringJDBCConfiguration.class);
		UserContactService pservice = (UserContactService) cont.getBean(UserContactService.class);
		Scanner sc = new Scanner(System.in);
		// pservice.insertData();
		// pservice.deleteData();
		System.out.println("******************************************************");
		System.out.println("******************************************************");
		System.out.println("........WELCOME TO CONTACT MANAGEMENT SYSTEM..........");
		System.out.println("******************************************************");
		System.out.println("******************************************************");
		System.out.println("User Contact Details Operations Using JDBC In Spring");
		System.out.println("......................................................");
		String ch = "y";
		while (ch.equals("y")) {
			System.out.println("1.Insert the Contact Details");
			System.out.println("2.Delete the Contact Details");
			System.out.println("3.Exit");
			System.out.println("Please Enter the Option");
			int option=sc.nextInt();
			switch (option) {
			case 1:
				System.out.println("Welcome to Insert the data");
				pservice.insertData();
				break;
			case 2:
				System.out.println("Welcome to Delete the data");
				pservice.deleteData();
				break;
			case 3:
				System.out.println("Successfully logout, Thank You!");
				System.exit(0);
				break;

			default:
				System.out.println("Invalid Option,Please Try again");

			}
			System.out.println("Do u want to continue the Contact Operations(y/n)");
			ch = sc.next();
		}
	}
}
