<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<title>Web Application Using Spring Boot</title>
</head>
<style>
body {
	font-family: Calibri;
	background-color: #c7ddcc;
}

td>form>label{
	border: 2px solid black;
	box-sizing: border-box;
	display: inline-block;
	padding: 12px 25px;
}
</style>

<body>
	<h1>ADD CONTACT DETAILS</h1>
	<form:form method="POST" action="saveData"
		modelAttribute="contactData">
		<table>
			<tr>
				<td><form:label path="id"><font color=Black size=5>User Id</font></form:label></td>
				<td><form:input path="id" /></td>
			</tr>
			<tr>
				<td><form:label path="name"><font color=Black size=5>Contact Name</font></form:label></td>
				<td><form:input path="name" /></td>
			</tr>
			<tr>
				<td><form:label path="email"><font color=Black size=5>Email</font></form:label></td>
				<td><form:input path="email" /></td>
			</tr>
			<tr>
				<td><form:label path="address"><font color=Black size=5>Address</font></form:label></td>
				<td><form:input path="address" /></td>
			</tr>
			<tr>
				<td><form:label path="telephone"><font color=Black size=5>Contact No</font></form:label></td>
				<td><form:input path="telephone" /></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Submit" /></td>
			</tr>
		</table>
	</form:form>
</body>

</html>