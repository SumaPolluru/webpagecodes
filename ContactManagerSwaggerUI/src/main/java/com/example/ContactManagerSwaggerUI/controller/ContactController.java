package com.example.ContactManagerSwaggerUI.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.ContactManagerSwaggerUI.dao.ContactDAO;
import com.example.ContactManagerSwaggerUI.model.Contact;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;

@RestController
public class ContactController {
	@Autowired
	ContactDAO pdao;
	@ApiOperation(value = "Contact List", 
		notes="This method will retreive list from database",nickname = "getContacts")
         @ApiResponses(value = {
	        @ApiResponse(code = 500, message = "Server error"),
	         @ApiResponse(code = 404, message = "Contact not found"),
	        @ApiResponse(code = 200, message = "Successful retrieval",
	            response = Contact.class, responseContainer = "List") })
         
	@GetMapping("/Retreive")
	public List<Contact> getProductList() {

		List<Contact> prdlist = pdao.getData();
		return prdlist;
	}

	@GetMapping("/RetreiveByID/{id}")
	public Contact getContactData(@PathVariable("id") int id) {
		Contact pdata = pdao.getById(id);
		return pdata;
	}
	@GetMapping("/DisplayForm")
	public ModelAndView getForm() {
		System.out.println("Hey,Displaying");
		return new ModelAndView("ContactForm", "contactData", new Contact());
	}
	@PostMapping("/saveData")
	public Contact saveData(@ModelAttribute("contactData") Contact p1)
	{
		pdao.save(p1);
		return p1;
	}


}
