package com.example.ContactManagerSwaggerUI.dao;

import org.springframework.stereotype.Component;

import com.example.ContactManagerSwaggerUI.model.Contact;


import java.util.*;
@Component
public class ContactDAO {
	private List<Contact> plist;
	public ContactDAO()
	{
		plist=new ArrayList<Contact>();
		plist.add(new Contact(101,"Suma","suma@gmail.com","Puttur","9381749152"));
		plist.add(new Contact(102,"Latha","Latha@gmail.com","Tirupati","9087766541"));
		plist.add(new Contact(103,"Sree","Sree@gmail.com","Nellore","9876551349"));
	}
	public void save(Contact p)
	{
		plist.add(p);
		System.out.println("Data saved");
	}
	public List<Contact> getData()
	{
		return plist;
	}
	public Contact getById(int id)
	{
		Contact pfind=null;
		boolean status=false;
		for(Contact p:plist)
		{
			if(p.getId()==id)
			{
				pfind=p;
				status=true;
			}
		}
		if(status)
		{
			System.out.println("Id is found");
		}
		else
		{
			System.out.println("Id is not found");
		}
		return pfind;
	}
	
}