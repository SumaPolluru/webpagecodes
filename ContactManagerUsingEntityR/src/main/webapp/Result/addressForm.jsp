<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<title>Web Application Using Spring Boot</title>
</head>
<style>
body {
	font-family: Calibri;
	background-color: #c7ddcc;
}

td>form>label{
	border: 2px solid black;
	box-sizing: border-box;
	display: inline-block;
	padding: 12px 25px;
}
</style>

<body>
	<h1>ADD ADDRESS DETAILS</h1>
	<form:form method="POST" action="addAddress"
		modelAttribute="addressdata">
		<table>
			<tr>
				<td><form:label path="addressId"><font color=Black size=5>Address Id</font></form:label></td>
				<td><form:input path="addressId" /></td>
			</tr>
			<tr>
				<td><form:label path="flatNumber"><font color=Black size=5>Flat Number</font></form:label></td>
				<td><form:input path="flatNumber" /></td>
			</tr>
			<tr>
				<td><form:label path="area"><font color=Black size=5>Area</font></form:label></td>
				<td><form:input path="area" /></td>
			</tr>
			<tr>
				<td><form:label path="city"><font color=Black size=5>City</font></form:label></td>
				<td><form:input path="city" /></td>
			</tr>
			<tr>
				<td><form:label path="zipcode"><font color=Black size=5>ZipCode</font></form:label></td>
				<td><form:input path="zipcode" /></td>
			</tr>			
			<tr>
				<td colspan="2"><input type="submit" value="Submit" /></td>
			</tr>
		</table>
	</form:form>
</body>

</html>