<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<title>Web Application Using Spring Boot</title>
</head>
<style>
body {
	font-family: Calibri;
	background-color: #c7ddcc;
}

td>form>label{
	border: 2px solid black;
	box-sizing: border-box;
	display: inline-block;
	padding: 12px 25px;
}
</style>

<body>
	<h1>ADD CONTACT DETAILS</h1>
	<form:form method="POST" action="addContact"
		modelAttribute="contactdata">
		<table>
			<tr>
				<td><form:label path="contactId"><font color=Black size=5>Contact Id</font></form:label></td>
				<td><form:input path="contactId" /></td>
			</tr>
			<tr>
				<td><form:label path="firstName"><font color=Black size=5>First Name</font></form:label></td>
				<td><form:input path="firstName" /></td>
			</tr>
			<tr>
				<td><form:label path="lastName"><font color=Black size=5>Last Name</font></form:label></td>
				<td><form:input path="lastName" /></td>
			</tr>
			<tr>
				<td><form:label path="mobileNumber"><font color=Black size=5>Mobile Number</font></form:label></td>
				<td><form:input path="mobileNumber" /></td>
			</tr>
			<tr>
				<td><form:label path="officeNumber"><font color=Black size=5>Office Number</font></form:label></td>
				<td><form:input path="officeNumber" /></td>
			</tr>
			<tr>
				<td><form:label path="emailId"><font color=Black size=5>Email Id</font></form:label></td>
				<td><form:input path="emailId" /></td>
			</tr>
			<tr>
				<td><form:label path="city"><font color=Black size=5>City</font></form:label></td>
				<td><form:input path="city" /></td>
			</tr>			
			<tr>
				<td colspan="2"><input type="submit" value="Submit" /></td>
			</tr>
		</table>
	</form:form>
</body>

</html>