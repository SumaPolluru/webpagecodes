<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<title>Web Application Using Spring Boot</title>
</head>
<style>
body {
	font-family: Calibri;
	background-color: #c7ddcc;
}

td>form>label {
	border: 2px solid black;
	box-sizing: border-box;
	display: inline-block;
	padding: 12px 25px;
}
</style>

<body>
	<h1>GET EMPLOYEE DETAILS</h1>
	<form:form method="POST" action="update" modelAttribute="coniddata">
		<table>
			<tr>
				<td><form:label path="contactId">
						<font color=Black size=5>Contact Id</font>
					</form:label></td>
				<td><form:input path="contactId" /></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="GetDetails" /></td>
			</tr>
		</table>
	</form:form>
</body>

</html>