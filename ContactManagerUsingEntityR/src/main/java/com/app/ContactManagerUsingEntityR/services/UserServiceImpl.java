package com.app.ContactManagerUsingEntityR.services;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.app.ContactManagerUsingEntityR.model.Role;
import com.app.ContactManagerUsingEntityR.model.User;
import com.app.ContactManagerUsingEntityR.model.UserRegistration;
import com.app.ContactManagerUsingEntityR.respositories.UserRepository;



@Service
public class UserServiceImpl implements UserService {

	private UserRepository userRepository;
   

	/*@Autowired
	private BCryptPasswordEncoder passwordEncoder;*/
	
	public UserServiceImpl(@Lazy UserRepository userRepository) {
		super();
		this.userRepository = userRepository;
	}

	@Override
	public User save(UserRegistration registrationDto) {
		User user = new User(registrationDto.getFirstName(), registrationDto.getLastName(), registrationDto.getEmail(),registrationDto.getPassword());
		return userRepository.save(user);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		User user = userRepository.findByEmail(username);

		if (user == null) {
			throw new UsernameNotFoundException("Invalid username or password![1]");
		}

		return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), null);
	}

}
