package com.app.ContactManagerUsingEntityR.services;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.app.ContactManagerUsingEntityR.model.User;
import com.app.ContactManagerUsingEntityR.model.UserRegistration;

public interface UserService extends UserDetailsService{

	User save(UserRegistration registrationDto);
	
}
