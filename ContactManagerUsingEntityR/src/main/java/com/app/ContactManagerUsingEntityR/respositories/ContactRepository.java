
package com.app.ContactManagerUsingEntityR.respositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.app.ContactManagerUsingEntityR.model.Contact;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Integer> {

	public List<Contact> findByOrderByFirstNameAsc();

	@Query("select con.firstName,con.lastName from Contact con order by lastName ASC")
	public List<String> findAllOrderByLastNameAsc();
	
	
}
