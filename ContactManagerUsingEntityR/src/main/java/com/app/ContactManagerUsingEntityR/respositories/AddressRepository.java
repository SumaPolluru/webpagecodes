package com.app.ContactManagerUsingEntityR.respositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.ContactManagerUsingEntityR.model.Address;

	@Repository
	public interface AddressRepository extends  JpaRepository<Address,Integer>
	{



	}
