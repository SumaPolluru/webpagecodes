package com.app.ContactManagerUsingEntityR.respositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.app.ContactManagerUsingEntityR.model.Contact;
import com.app.ContactManagerUsingEntityR.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	User findByEmail(String email);
	@Query("select count(contact_id) from Contact  where user_id=?1")
	public List<Long> getConList(int user_id);

	@Query("select con from Contact con where mobile_number  Like '7777%' ")
	public List<Contact> getContacts();

	@Query("select con from Contact con where first_name  Like '%Ajay%' ")
	public List<Contact> getAjayData();

	@Query("select con from Contact con where first_name  Like '%Ajay%' and city='delhi' ")
	public List<Contact> getAjayDelhiData();

	/*
	 * DML COMMANDS NOT SUPPORTED @Query("delete from Contact  where user_id=?1")
	 * public List<Contact> getDeleteId(int user_id);
	 */

}
