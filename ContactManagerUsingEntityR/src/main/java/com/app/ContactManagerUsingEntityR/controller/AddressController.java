package com.app.ContactManagerUsingEntityR.controller;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.app.ContactManagerUsingEntityR.model.Address;
import com.app.ContactManagerUsingEntityR.respositories.AddressRepository;
import com.app.ContactManagerUsingEntityR.respositories.UserRepository;

@Controller
public class AddressController {

	@Autowired
	AddressRepository adrep;

	@RequestMapping("/InsertAddress")
	public ModelAndView getForm() {
		return new ModelAndView("addressForm", "addressdata", new Address());
	}

	@RequestMapping("/addAddress")
	public String data(@ModelAttribute("addressdata") Address con) {
		adrep.save(con);// it will insert data in database.
		return "success";
	}

	@RequestMapping("/AddressDetails")
	public ModelAndView getDetails() {
		List<Address> adlist = adrep.findAll();
		return new ModelAndView("retrieveAddress", "adata", adlist);
	}

	@RequestMapping("/UpdateById")
	public ModelAndView getEmpDetails() {
		return new ModelAndView("updatebyIdForm", "adiddata", new Address());
	}

	@RequestMapping("/updateAddress")
	public ModelAndView update(@ModelAttribute("adiddata") Address e) {

		Optional<Address> cobj = adrep.findById(e.getAddressId());// It will insert data in databse.
		Address efind = null;
		if (cobj.isPresent()) {
			efind = cobj.get();
		}
		return new ModelAndView("updateAddressForm", "uobj", efind);

	}

	@RequestMapping("/saveAddressUpdation")
	public ModelAndView saveUpdates(@ModelAttribute("uobj") Address address) {
		adrep.save(address);
		return new ModelAndView("success");
	}

	@RequestMapping("/AddressdeleteById")
	public ModelAndView deleteById() {

		return new ModelAndView("addressdeleteForm", "addr", new Address());
	}

	@RequestMapping("/Addressdelete")
	public ModelAndView deletedata(@ModelAttribute("addr") Address address) {
		adrep.deleteById(address.getAddressId());
		return new ModelAndView("success");
	}

}
