package com.app.ContactManagerUsingEntityR.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {

	@GetMapping("/login")
	public String login() {
		return "login";
	}

	@GetMapping("/")
	public String home() {
		return "index";
	}
	@GetMapping("/sample")
	public String sample() {
		return "NewFile";
	}

}
