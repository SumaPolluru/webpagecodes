package com.app.ContactManagerUsingEntityR.controller;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.app.ContactManagerUsingEntityR.model.UserRegistration;
import com.app.ContactManagerUsingEntityR.services.UserService;

@Controller

public class UserRegistrationController {

	private UserService userService;

	public UserRegistrationController(@Lazy UserService userService) {
		super();
		this.userService = userService;
	}

	@ModelAttribute("user")
	public UserRegistration userRegistrationDto() {
		return new UserRegistration();
	}
	
	@RequestMapping("/registration")
	public String showRegistrationForm() {
		return "registration";
	}

	@PostMapping
	public String registerUserAccount(@ModelAttribute("user") UserRegistration registrationDto) {
		userService.save(registrationDto);
		return "redirect:/registration?success";
	}

}
